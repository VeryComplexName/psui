﻿using Microsoft.Owin.Hosting;
using System;
using System.Net.Http;
using UniversalHelper.Common.EventArguments;
using Newtonsoft.Json;
using UniversalHelper.Common.Config;
using System.Windows.Forms;
using UniversalHelper.ApiClient;
using System.Configuration;
using UniversalHelper.Common.Model;
using UniversalHelper.Common;
using System.IO;

namespace UniversalHelper.ProcessingEvents
{
    public class EventHandlers
    {
        MyConfig cfg = null;
        IDisposable webApp = null;
        IAbout app = null;
        IMain main = null;
        Timer timer = null;
        MyClient myClient = null;

        public MyConfig ReadCfg()
        {
            MyConfig cfg;
            
            cfg = new MyConfig();
            cfg.web = new WebCfg();
            cfg.web.httpProtocol = ConfigurationManager.AppSettings["webServerProtocol"] != null ? ConfigurationManager.AppSettings["webServerProtocol"] : "http";
            cfg.web.httpHost = ConfigurationManager.AppSettings["webServerHost"] != null ? ConfigurationManager.AppSettings["webServerHost"] : "localhost";
            cfg.web.httpPort = ConfigurationManager.AppSettings["webServerPort"] != null ? int.Parse(ConfigurationManager.AppSettings["webServerPort"]) : 9000;
            cfg.web.httpPrefix = ConfigurationManager.AppSettings["webServerPrefix"] != null ? ConfigurationManager.AppSettings["webServerPrefix"] : "api/v1";
            cfg.web.httpRepository = ConfigurationManager.AppSettings["webServerRepository"] != null ? ConfigurationManager.AppSettings["webServerRepository"] : "repository";

            return cfg;
        }

        // Event Handlers

        // Start Work
        public void StartWork(object sender, LogicStartEventArgs e)
        {
            app = (IAbout)sender;
            if (e != null)
            {
                main = e.main;
                timer = e.timer;
            }

            cfg = ReadCfg();
            
            webApp = WebApp.Start<UniversalHelper.Web.Api.Startup>(url: cfg.serverBind);

            myClient = new MyClient(cfg);
            //var ret = await client.GetAllAvailableRepo();
        }

        // Stop Work
        public void StopWork(object sender, LogicStopEventArgs e)
        {
            try { 
                if (webApp != null) { webApp.Dispose(); } 
            }
            catch { }

            if (app != null) { app.CloseApp(); }
        }

        // Clone Repo
        public async void CloneRepo(object sender, LogicCloneRepoArgs e)
        {
            /*
            LogicCloneRepoArgs e:
            ---------------------
            public System.Windows.Forms.Form ui;
            public object events; //MyEvents
            public string remoteRepo;
            public string localRepo;
            */

            //MyClientStatus tmp = await myClient.GetAllRepo();

            string dstPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            MyClientStatus tmp = await myClient.GetRepo(e.repoName, dstPath);
            
            if (tmp.Rc == 0) { main.Msg("sdsdsds s dsd s ds sds"); }
        }
    }
}
