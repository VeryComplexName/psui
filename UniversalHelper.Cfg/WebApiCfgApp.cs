﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UniversalHelper.Web.Api.Common;

namespace UniversalHelper.Web.Api.Cfg
{
    public class WebApiCfgApp : IWebApiCfgApp
    {
        public string Get(string Name)
        {
            string ret = ConfigurationManager.AppSettings[Name];
            if (string.IsNullOrEmpty(ret))
            {
                switch (Name.ToLower())
                {
                    case "webserverscriptsrepo": return @"https://gitlab.com/VeryComplexName/uhscripts.git";
                    case "webservercfgname": return @"webCfg.json";
                    case "webserverlogpath": return @"c:\temp\webLog.txt";
                    case "webserverprotocol": return @"http";
                    case "webserverhost": return @"localhost";
                    case "webserverport": return @"9000";
                    case "webserverprefix": return @"api/v1";
                    case "webserverrepository": return @"repository";
                    case "webserverworkingdirname": return "UH";
                }
            }
            return ret;
        }

        
    }
}
