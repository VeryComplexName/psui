﻿using System.Collections;
using System.Collections.Generic;
using UniversalHelper.Common.Model;
using UniversalHelper.Web.Api.Common;

namespace UniversalHelper.Web.Api.Cfg
{
    public interface IWebApiCfgWeb
    {
        string GetScriptsFolderName();
        string GetIconsFolderName();
        string GetModulesFolderName();
        string GetWorkingFolder();
        string GetScriptsRepoFolder();
        string GetArcPath();
        MyWebApiCfg Cfg(bool reRead = false, bool clone = true);
        List<MyRepo> Repositories(bool clone = true);
        MyRepo Repositories(string Name, bool clone = true);
        List<MyPsAction> Actions(bool clone = true);
        MyPsAction Actions(string Name, bool clone = true);

        object Clone(object cloneFrom);
        string RepositoryZipAndBase64(MyRepo repo);
        MyPsAction ActionsZipAndBase64(MyPsAction action);
        List<MyPsAction> ActionsZipAndBase64(List<MyPsAction> actions);
        MyWebApiCfg CfgZipAndBase64();
    }
}