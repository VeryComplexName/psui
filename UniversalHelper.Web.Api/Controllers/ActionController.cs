﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Management.Automation;
using System.Web.Http;
//using System.Web.Mvc;
using UniversalHelper.Common;
using UniversalHelper.Ps;
using Newtonsoft.Json;
using UniversalHelper.Web.Api.Models;
using System;
using UniversalHelper.Web.Api.Common;
using System.IO;
using UniversalHelper.Common.Model;
using System.Configuration;
using System.Reflection;
using UniversalHelper.Web.Api.Cfg;
using System.Linq;
using System.Text;

namespace UniversalHelper.Web.Api
{
    public class ActionController : ApiController
    {
        IRepo _repo = null;
        IWebApiCfgWeb _webApiCfgWeb = null;
        IWebApiCfgApp _webApiCfgApp = null;
        const string ControllerName = "Action";

        public ActionController(IWebApiCfgWeb webApiCfgWeb, IWebApiCfgApp webApiCfgApp, IRepo repo)
        {
            _repo = repo;
            _webApiCfgWeb = webApiCfgWeb;
            _webApiCfgApp = webApiCfgApp;
        }

        // GET api/v1/action
        // 1. Download repo
        // 2. Switch selected branch
        // 3. Zip repo
        // 4. Send archive back
        [HttpGet]
        public System.Web.Http.Results.JsonResult<MyStatus> Get(string version)
        {
            MyStatus result = new MyStatus();

            if (!VersionTemplateInUrl.Check(ControllerName, version)) { throw new Exception($"Api version has incorrect format [{version}]"); }

            try
            {
                var repo = _webApiCfgWeb.Actions();
                if (repo == null) { throw new Exception($"There are no Actions configured"); }

                result.Data = repo;
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            return Json(result);
        }

        // GET api/v1/action/myAction
        // 1. Download repo
        // 2. Zip repo
        // 3. Send archive back
        [HttpGet]
        //[ActionName("Repo")]
        public System.Web.Http.Results.JsonResult<MyStatus> Get(string version, string name)
        {
            MyStatus result = new MyStatus();

            if (!VersionTemplateInUrl.Check(ControllerName, version)) { throw new Exception($"Api version has incorrect format [{version}]"); }

            try
            {
                var action = _webApiCfgWeb.Actions(name);
                if (action == null) { throw new Exception($"There is no such Action name known as [{name}]"); }

                // Zip & Compress Actions: Script, Modules & Icons
                result.Data = _webApiCfgWeb.ActionsZipAndBase64(action);
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            return Json(result);
        }
    }
}