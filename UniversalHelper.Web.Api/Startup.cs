﻿// https://docs.microsoft.com/ru-ru/aspnet/web-api/overview/hosting-aspnet-web-api/use-owin-to-self-host-web-api
// Install-Package Microsoft.AspNet.WebApi.OwinSelfHost
// https://stackoverflow.com/questions/43311099/how-to-create-dependency-injection-for-asp-net-mvc-5

using Owin;
using System.Web.Http;
using Microsoft.Extensions.DependencyInjection;
using System.Web.Http.Dependencies;
using UniversalHelper.Web.Api.Models;
using System.Configuration;

namespace UniversalHelper.Web.Api
{
    public class Startup
    { 
        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {
            var logPath = ConfigurationManager.AppSettings["webServerLogPath"];
            if (string.IsNullOrEmpty(logPath)) { }

            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "Branch",
                routeTemplate: "api/{version}/{controller}/{name}/{branch}",
                defaults: new { name = RouteParameter.Optional, branch = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "Repo",
                routeTemplate: "api/{version}/{controller}/{name}",
                defaults: new { name = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "Repos",
                routeTemplate: "api/{version}/{controller}"
            );

            appBuilder.UseWebApi(config);

            // create the DI services and make the default resolver
            var services = new ServiceCollection();
            ConfigureServices.Configure(services);

            //services.AddTransient(typeof(MyStatus));
            //services.AddTransient(typeof(ValuesController));

            var resolver = new MyDependencyResolver(services.BuildServiceProvider());
            config.DependencyResolver = resolver;
        }
    }
}