﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UniversalHelper.Common.Config;
using UniversalHelper.Common.Model;
using Newtonsoft.Json;
using UniversalHelper.Common;
using System.IO;

namespace UniversalHelper.ApiClient
{
    public class MyClient : IDisposable
    {
        public HttpClient client = null;
        public MyConfig cfg = null;

        public MyClient(MyConfig cfg)
        {
            client = new HttpClient();
            this.cfg = cfg;
        }

        public async Task<MyClientStatus> GetAllRepo()
        {
            var res = await client.GetAsync(cfg.repositoryUrl);
            MyStatus myStatus = new MyStatus(Constants.MyStatusRc.ERROR);

            if (res != null & res.StatusCode == System.Net.HttpStatusCode.OK)
            {
                // res.Content : System.Net.Http.StreamContent
                var str = await res.Content.ReadAsStringAsync();
                myStatus = JsonConvert.DeserializeObject<MyStatus>(str);
                myStatus.Data = JsonConvert.DeserializeObject<List<MyRepo>>(myStatus.Data.ToString());
            }
            else { 
            }

            return new MyClientStatus(myStatus, AvailableAction.RepositoryList);
        }

        public async Task<MyClientStatus> GetRepo(string name, string dstDir)
        {
            var res = await client.GetAsync(cfg.repositoryUrl + $"/{name}");
            MyStatus myStatus = new MyStatus(Constants.MyStatusRc.ERROR);

            if (res != null & res.StatusCode == System.Net.HttpStatusCode.OK)
            {
                // res.Content : System.Net.Http.StreamContent
                var str = await res.Content.ReadAsStringAsync();
                myStatus = JsonConvert.DeserializeObject<MyStatus>(str);
                string extractTo = Path.Combine(dstDir, name);
                if (!Compression.Unbase64AndUnzip(myStatus.Data.ToString(), extractTo)) { throw new Exception($"Can't decompress returned data to [{extractTo}]"); }
            }
            else
            {
            }

            return new MyClientStatus(myStatus, AvailableAction.CloneRepository);
        }

        public void Dispose()
        {
            if(client != null) { client.Dispose(); }
        }
    }
}
