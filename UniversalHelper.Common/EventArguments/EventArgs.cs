﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversalHelper.ApiClient;

namespace UniversalHelper.Common.EventArguments
{ 
    public class LogicStartEventArgs : System.EventArgs
    {
        public object events; //MyEvents
        public System.Windows.Forms.Timer timer;
        public IMain main;
        public MyClient client;
    }

    public class LogicStopEventArgs : System.EventArgs
    {
        public string m_id;
    }

    public class LogicCloneRepoArgs : System.EventArgs
    {
        public System.Windows.Forms.Form ui;
        public object events; //MyEvents
        public string repoName;
        public string localFolder;
    }
}
