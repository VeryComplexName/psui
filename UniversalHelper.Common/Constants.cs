﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalHelper.Common
{
    public class Constants
    {
        public static class PSReturnCodes
        {
            public const int ok = 0;
            public const int error = -1;
            public const int exception = -2;
            public const int longrunning = -99;
            public const int fatal = -100;
            public const int prepared = -999;
            public const int notrunned = -1000;
        }
        public static class PSRetObjNames
        {
            public const string Error = "Error";
            public const string Result = "Result";
        }

        public static class PSRetErrorNames
        {
            public const string Code = "ErrorCode";
            public const string Description = "ErrorDescription";
        }

        public enum MyStatusRc
        {
            OK,
            ERROR,
        }

    }
}
