﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversalHelper.Common.Model;

namespace UniversalHelper.Common
{
    public class RepoBase : IRepoBase
    {
        public void WipeDir(string dir)
        {
            if (!string.IsNullOrEmpty(dir) & Directory.Exists(dir))
            {
                foreach (var f in Directory.EnumerateFiles(dir, "*.*", SearchOption.AllDirectories)) { File.SetAttributes(f, FileAttributes.Normal); }
                Directory.Delete(dir, true);
            }
        }

        public MyStatus CompressFile(string[] srcFile, string arcPath, bool wipeSrcFile = false)
        {
            MyStatus result = new MyStatus();

            try
            {
                if (!Compression.Zip(srcFile, arcPath)) { throw new Exception($"Error during compress file(s) to [{arcPath}]"); }
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            finally
            {
                if (wipeSrcFile)
                {
                    foreach (var f in srcFile)
                        if (File.Exists(f)) { File.Delete(f); }
                }
            }
            return result;
        }

        public MyStatus DecompressFile(string arcPath, string dstDir, bool wipeArc = false)
        {
            MyStatus result = new MyStatus();

            try
            {
                if (!Compression.Unzip(arcPath, dstDir, wipeArc)) { throw new Exception($"Error during uncompress file(s) from [{arcPath}]"); }
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            return result;
        }

        public MyStatus Unbase64AndDecompress(string base64string, string dstDir)
        {
            MyStatus result = new MyStatus();

            try
            {
                if (!Compression.Unbase64AndUnzip(base64string, dstDir)) { throw new Exception($"Error during uncompress file(s) from [{base64string}] to [{dstDir}]"); }
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            return result;
        }

        public MyStatus CompressFileAndBase64(string[] srcFile, string arcPath, bool wipeSrcFile = false, bool wipeArc = true)
        {
            MyStatus result = new MyStatus();

            try
            {
                result.Data = Compression.ZipAndBase64(srcFile, arcPath, wipeArc);
                if (result.Rc != 0) { throw new Exception("Error(s) happened during file(s) compression"); }
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            finally
            {
                if (wipeSrcFile)
                {
                    foreach (var f in srcFile)
                        if (File.Exists(f)) { File.Delete(f); }
                }
            }
            return result;
        }

        public MyStatus CompressFile(string srcFile, string arcPath, bool wipeSrcFile = false)
        {
            MyStatus result = new MyStatus();

            try
            {
                if (!Compression.Zip(srcFile, arcPath)) { throw new Exception($"Error during compress file(s) to [{arcPath}]"); }
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            finally
            {
                if (wipeSrcFile)
                {
                    if (File.Exists(srcFile)) { File.Delete(srcFile); }
                }
            }
            return result;
        }

        public MyStatus CompressFileAndBase64(string srcFile, string arcPath, bool wipeSrcFile = false, bool wipeArc = true)
        {
            MyStatus result = new MyStatus();

            try
            {
                result.Data = Compression.ZipAndBase64(srcFile, arcPath, wipeArc);
                if (result.Rc != 0) { throw new Exception("Error(s) happened during file(s) compression"); }
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            finally
            {
                if (wipeSrcFile)
                {
                    if (File.Exists(srcFile)) { File.Delete(srcFile); }
                }
            }
            return result;
        }

        public MyStatus Compress(string srcDir, string arcPath, bool wipeSrcDir = true)
        {
            MyStatus result = new MyStatus();

            try
            {
                if (!Compression.ZipFolder(srcDir, arcPath)) { throw new Exception($"Error compressing folder [{srcDir}] to the archive [{arcPath}]"); }
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            finally
            {
                if (wipeSrcDir) WipeDir(srcDir);
            }
            return result;
        }

    }
}
