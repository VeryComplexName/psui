﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace UniversalHelper.Common.Model
{
    public enum MyPsActionParamaterType 
    {
        String, Int, Float, Date, DateTime, ListSingleSelect, ListMultiSelect, OptionSet, CheckBoxSet
    }

    [Serializable]
    public class MyPsModule
    {
        public string Name;
        public string Content;
    }

    [Serializable]
    public class MyPsActionParamater
    {
        public MyPsActionParamaterType Type;
        public List<string> Names;
    }

    [Serializable]
    public class MyPsAction
    {
        public string Name;
        public string Script;
        public List<MyPsActionParamater> Parameters;
        public string Icon;
        public List<MyPsModule> Modules;
    }

    [Serializable]
    public class MyRepo
    {
        public string Name;
        public string Url;
        public string Branch;
    }

    [Serializable]
    public class MyWebApiCfg
    {
        public List<MyRepo> Repositories;
        public List<MyPsAction> Actions;
    }
}
