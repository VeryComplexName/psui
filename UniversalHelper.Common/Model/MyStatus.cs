﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static UniversalHelper.Common.Constants;

namespace UniversalHelper.Common.Model
{
    public class MyStatus
    {
        public MyStatusRc Rc;
        public string Msg;
        public object Data;

        public MyStatus()
        {
            Set();
        }

        public MyStatus(MyStatusRc rc = Constants.MyStatusRc.OK, string msg = "", object data = null)
        {
            Set(rc, msg, data);
        }

        public void Set(MyStatusRc rc = Constants.MyStatusRc.OK, string msg = "", object data = null)
        {
            Rc = rc;
            Msg = msg;
            Data = data;
        }
    }
}
