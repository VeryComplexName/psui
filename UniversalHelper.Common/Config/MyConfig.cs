﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalHelper.Common.Config
{
    public class WebCfg
    {
        public string httpProtocol;
        public string httpHost;
        public int httpPort;
        public string httpPrefix;
        public string httpRepository;
    }
    public class MyConfig
    {
        public WebCfg web;
        public string repositoryUrl { 
            get {return $"{this.web.httpProtocol}://" + $"{this.web.httpHost}:{this.web.httpPort}/{this.web.httpPrefix}/{this.web.httpRepository}".Replace("//", "/");}
        }
        public string serverBind
        {
            get { return $"{this.web.httpProtocol}://" + $"{this.web.httpHost}:{this.web.httpPort}".Replace("//", "/"); }
        }
    }
}
